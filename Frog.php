<?php
require_once('animal.php');

class Frog extends Animal {
    public $legs = 4;
    public $jump = "hop hop";
    public function jump() {
        echo $this->jump;
    }
}

